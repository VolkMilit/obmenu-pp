/*
 * leave all hope, if you see this code, you will probably die, you was been warned
 */

#include <vector>
#include <regex>
#include <algorithm>
#include <experimental/filesystem>

#include "config.h"
#include "ftlip/ftlip.h"
#include "obmenugenerator.h"

namespace fs = std::experimental::filesystem;

// macros		
#define MNAME std::string name = ini.get("Name[en_GB]"); \
						   if (name.empty()) name = ini.get("Name");

int main()
{
	obmenugenerator menu;
	
    int catid=0;
    
    std::vector<std::string> trans = {"Мультимедия", "Разработка", "Игры", "Графика", "Сеть", "Оффис", "Настройки", "Система", "Утилиты"};
    int transI = 0;

    std::vector<std::string> cat = CATEGORIES;
    std::vector<std::string> path = DESKTOP_PATHS;
    std::vector<std::string> a_hide = HIDE;
    bool hide = false;
    
    std::vector<std::string> tofind = {"Categories", "NoDisplay", "Icon", "Exec"};
    
    std::cout << "<openbox_pipe_menu>" << std::endl;
    std::cout << "<separator label=\"OPENBOX MENU\"/>" << std::endl;
    
    USER_DEFINE // see config.h
	std::cout << "<separator/>" << std::endl;
	
	// lastes files
	#if DISPLAY_LASTES == 1
		int lineCount = 0;
		
		//if (!fs::exists(HOME + "/.cache/obmenupp-last"))
		//{
			std::ifstream is(menu.HOME + "/.cache/obmenupp-last");
			std::string str;
	
			while(getline(is, str))
			{
				lineCount += 1;
				if (!fs::exists(str)) continue;
		
				ftlip ini(str);
        
				MNAME
			
				if (name.length() >= 15)
					name = name.erase(15, name.length()) + " ...";
														 
				menu.element( \
					ini.get("Icon"), \
					name, \
					std::regex_replace(ini.get("Exec"), std::regex(" %[A-z]"), "") \
				);
			}
			
			if (lineCount >= 2)
				std::cout << "	<separator/>" << std::endl;
		//}
	#endif
	//------------
    
	for (const auto & i:cat)
	{
		catid++;
		std::cout << "<menu icon='" << menu.catIcon(i) << "' id='" << catid << "' label='" << trans[transI] << "'>" << std::endl;
		transI++;
		
		for (const auto & j:path)
		{
			if (!fs::exists(j)) continue;
			
			for (auto & p : fs::directory_iterator(j))
			{
				ftlip ini(p.path());			
				std::size_t found = ini.get("Categories").find(i);

				if (found != std::string::npos)
				{
					MNAME
					
					for (auto hh : a_hide)
					{
						if (name == hh) 
						{
							hide = true;
							break;
						}
					}
					
					if (!hide && ini.get("NoDisplay") != "true")
					{				
						menu.element( \
							ini.get("Icon"), \
							name, \
							"lastobmenu " + std::string(p.path()) + " " + std::regex_replace(ini.get("Exec"), std::regex(" %[A-z]"), "") \
						);
					}
					
					hide = false;
				}
			}
		}

		std::cout << "</menu>" << std::endl;
	}
	
	std::cout << "<separator />" << std::endl;
	
	USER_DEFINE2 // see config.h

    std::cout << "</openbox_pipe_menu>" << std::endl;
    
    return 0;
}
